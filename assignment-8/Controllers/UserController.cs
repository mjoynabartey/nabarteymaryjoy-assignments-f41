﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using assignment_8.Models;

namespace assignment_8.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        [HttpGet]
        public ActionResult Register(int id = 0)
        {
            account acc = new account();

            return View(acc);
        }
        [HttpGet]
        public ActionResult Login() {
            
            return View();
        }

        public ActionResult Welcome()
        {
            return View();
        }

            public ActionResult Login(account accountModel)
        {
            using (AccountModels account = new AccountModels())
            {

                if (account.accounts.Any(x => x.username == accountModel.username))
                {
                    if (account.accounts.Any(x => x.password == accountModel.password))
                    {
                        ViewBag.Message = "Congrats naka Login kana";
                        return View("Welcome", accountModel);
                    }
                    else {
                        ViewBag.Message = "Okie na Sana Pero Mali kapadin ng Pinasok na Password";
                        return View("Login", accountModel);
                    }

                }
                else {

                    ViewBag.Message = "Mali ka nang Pinasok na Username";
                    return View("Login", accountModel);
                }
               
            }

        }

        [HttpPost]
        public ActionResult Register(account accountModel) {
            using (AccountModels account = new AccountModels()) {

                if (account.accounts.Any(x => x.username == accountModel.username)) {

                    ViewBag.DuplicateMessage = "Username is Already Taken! SANA ALL";
                    return View("Register", accountModel);
                }
                account.accounts.Add(accountModel);
                account.SaveChanges();
            }
            ModelState.Clear();
            ViewBag.SuccessMessage = "Register Kana";
            return View("Register", new account());
        }
    }
}