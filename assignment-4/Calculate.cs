﻿using System;

namespace assignment_4
{

    //Basic Calculation interface
    interface ICalculation{
        void BasiCalculation();
    }

    //Basic Calculation Class
    class BasicCalc : ICalculation{
        public void BasiCalculation(){
            Console.WriteLine("Basic Calculation");
            string fNumber;
            string sNumber;
            string Operation;

            Console.Write("\nEnter First Number\n");
            fNumber = Console.ReadLine();
            double fNum = Convert.ToDouble(fNumber);

            Console.Write("\nEnter Second Number\n");
            sNumber = Console.ReadLine();
            double sNum = Convert.ToDouble(sNumber);

            Console.Write("Choose Operation: 1(Addition), 2(Subtraction), 3(Multiplication), 4(Division)");
            Operation = Console.ReadLine();
            int basicOperation = Convert.ToInt32(Operation);

            switch (basicOperation){
                case 1:
                    Console.WriteLine("\nAnswer is : {0} \n", fNum + sNum); 
                    break;

                case 2:
                    Console.WriteLine("\nAnswer is : {0} \n", fNum - sNum); 
                    break;
                
                case 3:
                    Console.WriteLine("\nAnswer is : {0} \n", fNum * sNum);     
                    break;
                case 4:
                    Console.WriteLine("\nAnswer is : {0} \n", fNum / sNum);     
                    break;
            }
        }
    }

    //Scintific Calculations Interface
    interface IScientific{
    void ScientificCalculation();
    }

    //Scientific Calculation Class
    class SciCal : IScientific{
        public void ScientificCalculation(){
            Console.WriteLine("Basic Calculation");
            string fNumber;
            string sciOperation;

            Console.Write("\nEnter Number\n");
            fNumber = Console.ReadLine();
            double fNum = Convert.ToDouble(fNumber);

            Console.Write("\nChoose Operation: 1(Tangent), 2(Cosine), 3(Sine) \n");
            sciOperation = Console.ReadLine();
            int Op = Convert.ToInt32(sciOperation);

            switch (Op){
                case 1:
                    double tan = (fNum * (Math.PI)) / 180;
                    Console.WriteLine(Math.Tan(tan));
                    break;

                case 2:
                    double cos = (fNum * (Math.PI)) / 180;
                    Console.WriteLine(Math.Cos(cos));
                    break;
                
                case 3:
                    double sin = (fNum * (Math.PI)) / 180;
                    Console.WriteLine(Math.Tan(sin));
                    break;
            }
        }
    }
    
    class Iconversion{
		public void conversion() {
			Console.WriteLine("Basic Conversion");
			int biVal, decVal = 0, baseVal = 1, rem;
			Console.Write("Enter Binary Value : ");  
            biVal = Convert.ToInt32(Console.ReadLine());

			while (biVal > 0) {
            rem = biVal % 10;
            decVal = decVal + rem * baseVal;
            biVal = biVal / 10 ;
            baseVal = baseVal * 2;
            }
			Console.Write("Decimal: "+decVal);
         	Console.ReadLine();

        } 
	}
    class Program
    {
        static void Main(string[] args)
        {
            BasicCalc calc = new BasicCalc();
            SciCal sc = new SciCal();
            Iconversion conv = new Iconversion();

            calc.BasiCalculationMethod();
            sc.ScientificCalculationMethod();
            conv.conversion();
        }
    }
}           
