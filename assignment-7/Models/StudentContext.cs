using Microsoft.EntityFrameworkCore;

namespace registration.Models
{
    public class StudentContext : DbContext{

        public StudentContext(DbContextOptions<StudentContext> options) : base(options)
        {

        }

        public DbSet<StudentModel> StudentModel {get; set; }
    }
}