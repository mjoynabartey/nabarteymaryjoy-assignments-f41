using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using FileBackend.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Collections;

namespace FileBackend.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly IWebHostEnvironment WebHostEnvironment;
        private readonly DataContext _context;

        public FileController(IWebHostEnvironment webHostEnvironment, DataContext context)
        {
            WebHostEnvironment = webHostEnvironment;
            _context = context;
        }

        public class FileUpload 
        {
            public IFormFile files {get; set;}
        }

        [HttpPost("{folderid}")]
        public async Task<string> NewFiles([FromForm] FileUpload objFile, Guid folderid)
        {
            if (objFile.files.Length > 0)
            {
                var fileName = Path.GetFileName(objFile.files.FileName);
                var fileExtension = Path.GetExtension(fileName);
                var newFileName = String.Concat(Convert.ToString(Guid.NewGuid()), fileExtension);

                if (!Directory.Exists(WebHostEnvironment.WebRootPath + "\\Upload\\"))
                {
                    Directory.CreateDirectory(WebHostEnvironment.WebRootPath + "\\Upload\\");
                }

                using (FileStream fileStream = System.IO.File.Create(WebHostEnvironment.WebRootPath + "\\Upload\\" + newFileName))
                {
                    objFile.files.CopyTo(fileStream);
                    fileStream.Flush();

                    var objfiles = new Files();
                    objfiles.Id = Guid.NewGuid();
                    objfiles.FolderId = folderid;
                    objfiles.FileName = objFile.files.FileName;
                    objfiles.File = newFileName;
                    objfiles.Date = DateTime.Now;

                    _context.Files.Add(objfiles);
                    await _context.SaveChangesAsync();
                    
                    return "\\Upload\\" + newFileName;
                }
            }
            else
            {
                return "Failed";
            }
        }

        [HttpGet("{folderid}")]
        public IEnumerable getTask(Guid folderid)
        {
            var files = from q in _context.Files
            where q.FolderId == folderid
            select q;
            
            return files;
        }

        
    }
}
