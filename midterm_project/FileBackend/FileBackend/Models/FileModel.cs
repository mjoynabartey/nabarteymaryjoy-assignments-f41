﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileBackend.Models
{
    public class Files
    {
        public Guid Id { get; set; }
        public Guid FolderId { get; set; }
        public string FileName { get; set; }
        public string File { get; set; }
        public DateTime Date { get; set; }
    }
}
