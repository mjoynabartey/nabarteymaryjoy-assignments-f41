using System;
using Microsoft.AspNetCore.Http;

namespace FileBackend.Models
{
    public class TaskImageViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string files { get; set; }
    }
}