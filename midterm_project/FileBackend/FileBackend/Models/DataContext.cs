﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace FileBackend.Models
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
        : base(options)
        {

        }

        public DbSet<Folder> Folder { get; set; }
        public DbSet<Files> Files { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<TaskImageViewModel> image {get; set;}
    }
}
