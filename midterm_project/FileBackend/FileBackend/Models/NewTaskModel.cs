using System;
using Microsoft.AspNetCore.Http;

namespace FileBackend.Models
{
    public class NewTaskModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public byte[] files { get; set; }
    }
}