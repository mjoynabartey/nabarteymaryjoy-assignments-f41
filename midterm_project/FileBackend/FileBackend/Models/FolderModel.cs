﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileBackend.Models
{
    public class Folder
    {
        public Guid Id { get; set; }
        public Guid Created { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
    }
}
