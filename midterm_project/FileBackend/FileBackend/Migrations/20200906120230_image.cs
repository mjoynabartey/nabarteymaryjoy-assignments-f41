﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FileBackend.Migrations
{
    public partial class image : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Image",
                table: "newTask");

            migrationBuilder.AddColumn<byte[]>(
                name: "DataFiles",
                table: "newTask",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FileType",
                table: "newTask",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DataFiles",
                table: "newTask");

            migrationBuilder.DropColumn(
                name: "FileType",
                table: "newTask");

            migrationBuilder.AddColumn<string>(
                name: "Image",
                table: "newTask",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);
        }
    }
}
