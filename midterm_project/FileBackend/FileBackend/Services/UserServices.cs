using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using FileBackend.Models;
using FileBackend.Helpers;

namespace FileBackend.Services
{
    public interface IUserServices
    {
        User Register(User user);
        User Login(string email, string password);
    }

    public class UserServices : IUserServices
    {
        private DataContext _context;
        private readonly AppSettings _appSettings;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public UserServices(DataContext context, IOptions<AppSettings> appSettings, IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _appSettings = appSettings.Value;
            _context = context;
        }

        public User Login(string email, string password)
        {   
            var user = _context.User.SingleOrDefault(x => x.Username == email && x.Password == password);

            if (user == null)
                return null;
                
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.key);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new System.Security.Claims.ClaimsIdentity(new Claim[]{
                    new Claim(ClaimTypes.Name, user.UserId.ToString()),
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.jwt = tokenHandler.WriteToken(token);

            user.Password = null;
            return user;
        }

        public User Register(User user)
        {
            if (_context.User.Any(x => x.Username == user.Username))
                throw new AppException("Username \"" + user.Username + "\" is already taken");

            _context.User.Add(user);
            _context.SaveChanges();

            return user;
        }
        
    }
}